<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('indek');
});
route::get('/form', function() {
    return view('form');
});
route::get('/welcome', function(){
    return view('welcome');
});
//route::get('/form', 'AuthConroller@form');
//route::get('/welcome', 'AuthConroller@welcome');